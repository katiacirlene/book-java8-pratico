package com.kate_testes;

public class Capitulo3 {

	public static void main(String[] args) {
		
		Validador<String> validadorCEP = valor -> valor.matches("[0-9]{5}-[0-9]{3}");
		boolean valida = validadorCEP.valida("20750-124");
		System.out.println(valida);
		
		Runnable o = () -> { System.out.println("O que sou eu? Que lambda?"); };
		System.out.println(o);
		System.out.println(o.getClass());
	}

}
