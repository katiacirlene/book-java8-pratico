package com.kate_testes;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class Capitulo5_1 {

	public static void main(String[] args) {

		List<LocalDate> datas = new ArrayList<>();
		datas.add(LocalDate.of(2017, 1, 10));
		datas.add(LocalDate.of(2017, 9, 01));
		
		datas.sort((d1, d2) -> d2.compareTo(d1));
		
		datas.forEach(d -> System.out.println(d.format(DateTimeFormatter.ofPattern("dd/MM/yyyy"))));		

	}

}
