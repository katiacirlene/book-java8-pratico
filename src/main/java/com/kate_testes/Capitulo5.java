package com.kate_testes;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.function.Function;
import java.util.function.ToIntFunction;

public class Capitulo5 {

	public static void main(String[] args) {
		Usuario user1 = new Usuario("Paulo Silveira", 150);
		Usuario user2 = new Usuario("Rodrigo Turini", 120);
		Usuario user3 = new Usuario("Guilherme Silveira", 190);
		
		List<Usuario> usuarios = new ArrayList<>();
		usuarios.add(user1);
		usuarios.add(user2);
		usuarios.add(user3);
		
		testeUsuarios(usuarios);
		
//		testePalavras();
		
//		testePontos(usuarios);
		
//		testePontosDesc(usuarios);
		
		testePontosComparingInt(usuarios);

	}
	
	public static void testeUsuarios(List<Usuario> usuarios) {
//		Comparator<Usuario> comparator = (u1, u2) -> u1.getNome().compareTo(u2.getNome());
//		Collections.sort(usuarios, comparator);
		
//		Collections.sort(usuarios, (u1, u2) -> u1.getNome().compareTo(u2.getNome()));
		
//		usuarios.sort((u1, u2) -> u1.getNome().compareTo(u2.getNome()));
		
//		Comparator<Usuario> comparator = Comparator.comparing(u -> u.getNome());
//		usuarios.sort(comparator);
		
		usuarios.sort(Comparator.comparing(u -> u.getNome()));
		
		usuarios.forEach(u -> System.out.println(u.getNome()));		
	}
	
	public static void testePalavras() {
		System.out.println("======================");
		
		List<String> palavras = Arrays.asList("Casa do Código", "Alura", "Caelum");

		palavras.sort(Comparator.naturalOrder());
//		palavras.sort(Comparator.reverseOrder());
		
		palavras.forEach(p -> System.out.println(p));
		
		System.out.println("======================");		
	}
	
	public static void testePontos(List<Usuario> usuarios) {
		System.out.println("======================");
		
		Function<Usuario, Integer> extraiPontos = u -> u.getPontos();
		Comparator<Usuario> comparator = Comparator.comparing(extraiPontos);
		
		usuarios.sort(comparator);
		usuarios.forEach(u -> System.out.println(u.getNome() + " - " + u.getPontos()));		
		
		System.out.println("======================");
	}
	
	public static void testePontosComparingInt(List<Usuario> usuarios) {
		System.out.println("======================");
		
		// Para evitar o Autoboxing
//		ToIntFunction<Usuario> extraiPontos = u -> u.getPontos();
//		Comparator<Usuario> comparator = Comparator.comparingInt(extraiPontos);
//		
//		usuarios.sort(comparator);
//		usuarios.forEach(u -> System.out.println(u.getNome() + " - " + u.getPontos()));	
		
		usuarios.sort(Comparator.comparingInt(u -> u.getPontos()));
		usuarios.forEach(u -> System.out.println(u.getNome() + " - " + u.getPontos()));	
		
		System.out.println("======================");
	}
	
	public static void testePontosDesc(List<Usuario> usuarios) {
		System.out.println("======================");
		
		Comparator<Usuario> comparator = (u1, u2) -> Integer.compare(u2.getPontos(), u1.getPontos());		
		
		usuarios.sort(comparator);		
		usuarios.forEach(u -> System.out.println(u.getNome() + " - " + u.getPontos()));			
		
		System.out.println("======================");		
	}

}
