package com.kate_testes;

import java.util.Arrays;
import java.util.List;

public class Capitulo2 {

	public static void main(String[] args) {
		Usuario user1 = new Usuario("Paulo Silveira", 150);
		Usuario user2 = new Usuario("Rodrigo Turini", 120);
		Usuario user3 = new Usuario("Guilherme Silveira", 190);
		
		List<Usuario> usuarios = Arrays.asList(user1, user2, user3);
		
//		for (Usuario usuario : usuarios) {
//			System.out.println(usuario.getNome());
//		}
		
//		usuarios.forEach(new Consumer<Usuario>() {
//			public void accept(Usuario usuario) {
//				System.out.println(usuario.getNome());
//			}
//		});
		
		usuarios.forEach(u -> System.out.println(u.getNome()));
		
	}

}
