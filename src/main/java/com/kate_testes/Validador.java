package com.kate_testes;

@FunctionalInterface
public interface Validador<T> {
	boolean valida(T t);
}
