package com.kate_testes;

public class Usuario {

	private String nome;
	private int pontos;
	private boolean moderador;
	
	public Usuario(String nome, int pontos) {
		this.nome = nome;
		this.pontos = pontos;
		this.moderador = false;
	}

	public String getNome() {
		return nome;
	}

	public int getPontos() {
		return pontos;
	}

	public boolean isModerador() {
		return moderador;
	}
	
	public void tornaModerador() {
		this.moderador = true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Usuario [nome=");
		builder.append(nome);
		builder.append(", pontos=");
		builder.append(pontos);
		builder.append(", moderador=");
		builder.append(moderador);
		builder.append("]");
		
		return builder.toString();
	}
	
}
