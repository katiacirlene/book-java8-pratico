package com.kate_testes;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class Capitulo6 {

	public static void main(String[] args) {
		Usuario user1 = new Usuario("Paulo Silveira", 150);
		Usuario user2 = new Usuario("Rodrigo Turini", 120);
		Usuario user3 = new Usuario("Guilherme Silveira", 190);
		
		List<Usuario> usuarios = new ArrayList<>();
		usuarios.add(user1);
		usuarios.add(user2);
		usuarios.add(user3);

//		usuarios.forEach(u -> System.out.println(u));
		usuarios.forEach(System.out::println);
		
//		tornaModerador(usuarios);
		
//		testeComparadorComplexo(usuarios);
		
//		testeOrdernarPontosDesc(usuarios);
	}

	public static void tornaModerador(List<Usuario> usuarios) {
		System.out.println("======================");
		
//		usuarios.forEach(u -> u.tornaModerador());
		usuarios.forEach(Usuario::tornaModerador);
		
		usuarios.forEach(u -> System.out.println(u));		
		
		System.out.println("======================");
	}
	
	public static void testeComparadorComplexo(List<Usuario> usuarios) {
		System.out.println("======================");
		
		Comparator<Usuario> c = Comparator.comparingInt(Usuario::getPontos).thenComparing(Usuario::getNome);
		usuarios.sort(c);
		
		usuarios.forEach(u -> System.out.println(u));
		
		System.out.println("======================");
	}
	
	public static void testeOrdernarPontosDesc(List<Usuario> usuarios) {
		System.out.println("======================");
		
		usuarios.sort(Comparator.comparing(Usuario::getPontos).reversed());
		
		usuarios.forEach(u -> System.out.println(u));
	}
	
}
