package com.kate_testes;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Capitulo7 {

	public static void main(String[] args) {
		
		Usuario user1 = new Usuario("Paulo Silveira", 150);
		Usuario user2 = new Usuario("Rodrigo Turini", 120);
		Usuario user3 = new Usuario("Guilherme Silveira", 90);
		
		List<Usuario> usuarios = new ArrayList<>();
		usuarios.add(user1);
		usuarios.add(user2);
		usuarios.add(user3);
		
//		tornandoModerador(usuarios);
		
//		filtrarMaisDe100Pontos(usuarios);
		
//		testeListaMaisDe100Pontos(usuarios);
		
//		testeSetMaisDe100Pontos(usuarios);
		
		testeListaDePontos(usuarios);

	}
	
	public static void tornandoModerador(List<Usuario> usuarios) {
		
		usuarios.sort(Comparator.comparing(Usuario::getPontos).reversed());
		
		Consumer<Usuario> tornaModerador = Usuario::tornaModerador;
		Consumer<Usuario> imprime = u -> System.out.println(u);
		
		usuarios
			.subList(0, 2)
			.forEach(tornaModerador.andThen(imprime));
	}
	
	public static void filtrarMaisDe100Pontos(List<Usuario> usuarios) {
		
//		Stream<Usuario> stream = 
//				usuarios
//					.stream()
//					.filter(u -> { return u.getPontos() > 100; });
		
//		Stream<Usuario> stream = 
//				usuarios
//					.stream()
//					.filter(u -> u.getPontos() > 100);
//		
//		stream.forEach(System.out::println);
		
		usuarios
			.stream()
			.filter(u -> u.getPontos() > 100)
			.forEach(System.out::println);
	}
	
	public static void testeListaMaisDe100Pontos(List<Usuario> usuarios) {

		List<Usuario> maisDe100 = 
				usuarios
					.stream()
					.filter(u -> u.getPontos() > 100)
					.collect(Collectors.toList());
		
		maisDe100.forEach(System.out::println);
	}
		
	public static void testeSetMaisDe100Pontos(List<Usuario> usuarios) {
		
//		Set<Usuario> maisDe100 = 
//				usuarios
//					.stream()
//					.filter(u -> u.getPontos() > 100)
//					.collect(Collectors.toSet());
		
		Set<Usuario> maisDe100 = 
				usuarios
					.stream()
					.filter(u -> u.getPontos() > 100)
					.collect(Collectors.toCollection(HashSet::new));
		
		maisDe100.forEach(System.out::println);
	}
	
	public static void testeListaDePontos(List<Usuario> usuarios) {
	
//		List<Integer> pontos = new ArrayList<>();
//		
//		usuarios.forEach(u -> pontos.add(u.getPontos()));
		
//		List<Integer> pontos = 
//				usuarios
//					.stream()
//					.map(u -> u.getPontos())
//					.collect(Collectors.toList());
		
		List<Integer> pontos = 
				usuarios
					.stream()
					.map(Usuario::getPontos)
					.collect(Collectors.toList());		
		
		pontos.forEach(System.out::println);
	}
		
}
